cmake_minimum_required(VERSION 3.16)

project(PowerDevil)
set(PROJECT_VERSION "5.27.80")
set(PROJECT_VERSION_MAJOR 5)

set(QT_MIN_VERSION "6.4.0")
set(KF6_MIN_VERSION "5.240.0")
set(KDE_COMPILERSETTINGS_LEVEL "5.82")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(ECM ${KF6_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH}  ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(ECMSetupVersion)
include(ECMQtDeclareLoggingCategory)
include(ECMConfiguredInstall)
include(FeatureSummary)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(KDEClangFormat)
include(ECMDeprecationSettings)

find_package(Qt${QT_MAJOR_VERSION} ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS Widgets DBus)
if (QT_MAJOR_VERSION EQUAL "5")
    find_package(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED X11Extras)
endif()
find_package(KF6 ${KF6_MIN_VERSION} REQUIRED COMPONENTS Activities Auth IdleTime Config DBusAddons Solid I18n GlobalAccel KIO NotifyConfig DocTools Crash Notifications Kirigami2 KCMUtils)
find_package(KF6Screen CONFIG REQUIRED)
find_package(LibKWorkspace CONFIG REQUIRED)

find_package(KF6BluezQt ${KF6_MIN_VERSION})
set_package_properties(KF6BluezQt
    PROPERTIES DESCRIPTION "Qt wrapper for BlueZ 5 DBus API"
    TYPE OPTIONAL
    PURPOSE "Support for wireless energy saving actions"
)
find_package(KF6NetworkManagerQt ${KF6_MIN_VERSION})
set_package_properties(KF6NetworkManagerQt
    PROPERTIES DESCRIPTION "Qt wrapper for NetworkManager API"
    TYPE OPTIONAL
    PURPOSE "Support for wireless energy saving actions"
)

set(HAVE_WIRELESS_SUPPORT FALSE)
if(KF6NetworkManagerQt_FOUND AND KF6BluezQt_FOUND)
    set(HAVE_WIRELESS_SUPPORT TRUE)
endif()
add_feature_info(
    "Wireless power saving"
    HAVE_WIRELESS_SUPPORT
    "Support turning off signal-transmitting devices to save energy"
)

find_package(LibKWorkspace ${PROJECT_VERSION} REQUIRED)

find_package(UDev REQUIRED)

find_package(XCB REQUIRED COMPONENTS XCB RANDR DPMS)

find_package(Libcap)
set_package_properties(Libcap PROPERTIES
    TYPE OPTIONAL
    PURPOSE "Needed for scheduled wakeup which can wake from suspend (CAP_WAKE_ALARM)"
)
set(HAVE_LIBCAP ${Libcap_FOUND})

find_package(DDCUtil)
if(DDCUtil_FOUND)
    add_definitions(-DWITH_DDCUTIL)
    set_package_properties(DDCUtil
        PROPERTIES DESCRIPTION "DDCUtil library support"
        TYPE OPTIONAL
        PURPOSE "Set monitor settings over DDC/CI channel"
    )
endif()

include_directories (
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/daemon
)

add_definitions(-DQT_NO_KEYWORDS)
add_definitions(-DQT_NO_FOREACH)

ecm_setup_version(${PROJECT_VERSION} VARIABLE_PREFIX POWERDEVIL
                  VERSION_HEADER "${CMAKE_BINARY_DIR}/powerdevil_version.h"
)
ecm_set_disabled_deprecation_versions(QT 5.15.2
    KF 5.101
)

add_subdirectory(daemon)
add_subdirectory(kcmodule)
add_subdirectory(doc)
kdoctools_install(po)

ecm_qt_install_logging_categories(
        EXPORT POWERDEVIL
        FILE powerdevil.categories
        DESTINATION ${KDE_INSTALL_LOGGINGCATEGORIESDIR}
        )
install( FILES powerdevil.notifyrc DESTINATION ${KDE_INSTALL_KNOTIFYRCDIR} )

# add clang-format target for all our real source files
file(GLOB_RECURSE ALL_CLANG_FORMAT_SOURCE_FILES *.cpp *.h)
kde_clang_format(${ALL_CLANG_FORMAT_SOURCE_FILES})

ki18n_install(po)

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
